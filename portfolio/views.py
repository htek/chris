from django.shortcuts import render

from models import Project


def index(request):
    project_list = Project.objects.order_by('-pub_date')
    context = {
        'project_list': project_list,
    }
    return render(request, 'portfolio/index.html', context)



