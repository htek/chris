from django.db import models
from django.core.files.storage import FileSystemStorage



class Project(models.Model):
	business_name = models.CharField(max_length=200)
	pub_date = models.DateTimeField('date published')
	description = models.CharField(max_length=500)
	screenshot = models.ImageField(upload_to='', max_length=100, null=True)	
	business_url = models.CharField(max_length=200, null=True)
	
	def __str__(self):
		return self.business_name
